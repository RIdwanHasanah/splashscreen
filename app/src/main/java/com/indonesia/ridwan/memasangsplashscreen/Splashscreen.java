package com.indonesia.ridwan.memasangsplashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.os.Handler;

public class Splashscreen extends AppCompatActivity {

    //set waktu lama Splashscreen
    private static int splashInterval = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(Splashscreen.this,MainActivity.class);
                startActivity(i); //Menghubunngkan activity splashscreen ke main activity dgn intent

                //jeda seelasi Splashscreen
                this.finish();
            }

            private void finish(){

            }
        },splashInterval);
    }
}
